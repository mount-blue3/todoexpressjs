const todo = require("../model/database.js");
const yup = require("yup");

const schema = yup.object().shape({
  text: yup.string().strict(true).required(),
  isCompleted: yup.boolean().strict(true).required(),
});
async function getTodos(req, res, next) {
  try {
    const result = await todo.findAll();
    res.status(200).json(result);
    return;
  } catch (error) {
    next(error);
  }
}

async function getTodosById(req, res, next) {
  try {
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
      res.status(400).json({ message: "Enter valid ID" });
      return;
    } else {
      const result = await todo.findByPk(id);
      if (!result) {
        res.status(404).json({ message: "No todo Found" });
        return;
      } else {
        res.status(200).json(result);
        return;
      }
    }
  } catch (error) {
    next(error);
  }
}
async function addTodo(req, res, next) {
  try {
    const { text, isCompleted } = req.body;
    const data = {
      text: text,
      isCompleted: isCompleted,
    };
    await schema.validate(data);
  } catch (err) {
    return res
      .status(400)
      .json({ error: err.message, message: "not a valid todo" });
  }
  try {
    const result = await todo.create(req.body);
    res.status(201).json(result);
  } catch (error) {
    next(error);
  }
}
async function deleteTodo(req, res, next) {
  try {
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
      res.status(400).json({ message: "Enter valid ID" });
      return;
    } else {
      const todo1 = await todo.findByPk(id);
      if (todo1 === null) {
        res.status(400).json({ message: "No todo with that id" });
        return;
      } else {
        await todo.destroy({
          where: {
            id: id,
          },
        });
        res.status(200).json({ message: `deleted todo with id ${id}` });
        return;
      }
    }
  } catch (error) {
    next(error);
  }
}

async function updateTodo(req, res, next) {
  try {
    await schema.validate(req.body);
  } catch (error) {
    res.status(400).json({ error: error.message, message: "Not a valid todo" });
    return;
  }
  try {
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
      res.status(404).json({ message: "eneter valid id" });
      return;
    }

    const todo1 = await todo.findByPk(id);
    if (todo1 === null) {
      res.status(404).json({ message: "No todo with that id" });
    } else {
      await todo.update(req.body, { where: { id: id } });
      res.status(200).json({ message: "updated todo" });
      return;
    }
  } catch (error) {
    next(error);
  }
}
async function getCompletedTodos(req, res, next) {
  try {
    const todos = await todo.findAll({ where: { isCompleted: true } });

    res.status(200).json(todos);
    return;
  } catch (error) {
    next(error);
  }
}
async function getFirstCompletedTodo(req, res, next) {}
module.exports = {
  getTodos,
  getTodosById,
  addTodo,
  deleteTodo,
  updateTodo,
  getCompletedTodos,
};
