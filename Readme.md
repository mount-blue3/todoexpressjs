# Todo RESTAPI

This is a simple REST API for managing todo items. It provides endpoints for creating, updating, deleting, and retrieving todo items.

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Node.js
- PostgreSQL

### Installing

**Clone the repository:**

git clone https://gitlab.com/mount-blue3/todoexpressjs.git

**Install the dependencies:**

cd todo-restful-api

```bash
npm install
```

**Configure the environment variables:**

cp .env.example .env

Update the .env file with your PostgreSQL database credentials.

**Set up the database:**

```bash
npm run db:migrate
```

**Start the server:**

```bash
npm start
```

By default, the server runs on http://localhost:4000.

### API Documentation

**Todos**
**Get all todos**

GET /todos/

Retrieves all todo items.

**Get a specific todo**

GET /todos/:id

Retrieves a specific todo item based on the provided id parameter.

**Create a todo**

POST /todos/

Creates a new todo item. The request body should contain the following JSON data:

{

"text": "Task description",

"isCompleted": false

}

**Update a todo**

PUT /todos/:id

Updates an existing todo item with the provided id. The request body should contain the updated JSON data:

{

"text": "Updated task description",

"isCompleted": true

}

**Delete a todo**

DELETE /todos/:id

Deletes the todo item with the provided id.

### Error Handling

If an error occurs during the API requests, the server will respond with an appropriate error message and status code. Common error codes include:

- **400 Bad Request:** Invalid request or missing required data.
- **404 Not Found:** The requested resource does not exist.
- **500 Internal Server Error:** An unexpected server error occurred.

### Built With

- Express.js - Web framework for Node.js
- Sequelize - ORM for interacting with the database
- Yup - Validation library
- PostgreSQL - Relational database management system
