const express = require("express");
const routes = require("./controller/routes.js");
const dotenv = require("dotenv");
dotenv.config();

const Sequelize = require("sequelize");
const todo = require("./model/database.js");
const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.USER_NAME,
  process.env.PASSWORD,
  {
    dialect: "postgres",
    host: "localhost",
  }
);
todo
  .sync()
  .then(() => {
    console.log("Database synced successfully.");
    return sequelize.authenticate();
  })
  .then(() => {
    console.log("Database connected successfully.");
  })
  .catch((err) => {
    console.error("Error syncing/ connecting to database:", err);
  });

const handle500Error = (err, req, res, next) => {
  res.status(500).json({ message: "Internal server error" });
};

const app = express();
const port = process.env.EXPRESS_JS_PORT || 4000;

app.use(express.json());
app.use("/todos", routes);
app.use(handle500Error);

app.listen(port, () => console.log(`app listening on port ${port}`));
