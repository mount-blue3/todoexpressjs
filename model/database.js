const { Sequelize } = require("sequelize");
const dotenv = require("dotenv");
dotenv.config();

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.USER_NAME,
  process.env.PASSWORD,
  {
    dialect: "postgres",
    host: "localhost",
  }
);
const todo = sequelize.define("todos", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  text: Sequelize.STRING,
  isCompleted: Sequelize.BOOLEAN,
});

module.exports = todo;
